﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dyd.BaseService.Monitor.Collect.BackgroundTasks;
using XXF.BaseService.Monitor;
using Dyd.BaseService.Monitor.Core;
using XXF.BaseService.Monitor.SystemRuntime;

namespace Dyd.BaseService.Monitor.Collect
{
    /// <summary>
    /// 被监控服务器监控数据采集的入口类，服务器监控服务运行时，会找到该程序集Dyd.BaseService.Monitor.exe中的CollectMonitorDll类实例化。
    /// 然后调用Start方法
    /// 
    /// 监控Dll（也就是该dll)主要任务：
    /// 1、集群服务器状态信息更新
    /// 2、集群服务器的性能计数器的数据采取
    /// 3、维护心跳，定时更新最后时间，表示该服务器监控服务在运行中。
    /// </summary>
    public class CollectMonitorDll:BaseCollectMonitorDll
    {
        public List<BaseBackgroundTask> BackgroundTasks = new List<BaseBackgroundTask>();

        /// <summary>
        /// 数据采集的入口点
        /// </summary>
        public override void Start()
        {
            //监控服务主库地址，其它地址从主库里得到，该地址从通过监控平台的web的一个Url请求得到
            CoreGlobalConfig.PlatformManageConnectString = this.PlatformManageConnectString;
            if (string.IsNullOrWhiteSpace(GlobalConfig.ServerIP))
                GlobalConfig.ServerIP = this.ServerIP;//监控服务载入时会读取本机IP
            GlobalConfig.LoadBaseConfig();//其它的库配置
            GlobalConfig.LoadClusterConfig();//当前服务器的监控配置
            if (BackgroundTasks.Count == 0)
            { 
                //下面为对应的三个主要任务
                BackgroundTasks.Add(new MonitorCollectBackgroundTask());
                BackgroundTasks.Add(new PerformanceCollectBackgroundTask());
                //BackgroundTasks.Add(new ConfigUpdateBackgroundTask());
                BackgroundTasks.Add(new OnLineTimeBackgroundTask());
                foreach (var t in BackgroundTasks)
                {
                    t.Start();
                }
            }
        }

    }
}
